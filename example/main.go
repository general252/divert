package main

import (
	"gitee.com/general252/divert"
	"log"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	h := divert.NewWinDivert()

	err := h.WinDivertOpen("true",
		divert.WINDIVERT_LAYER_NETWORK,
		divert.WINDIVERT_PRIORITY_NORMAL,
		divert.WINDIVERT_FLAG_SNIFF,
	)
	if err != nil {
		log.Println(err)
		return
	}
	defer h.WinDivertClose()

	log.Println("open success")

	for {
		data, addr, err := h.ReadNetwork()
		if err != nil {
			break
		}

		_ = data
		if iface, ok := addr.Interface(); ok {
			log.Println(iface.Name, len(data))
		}
	}

}
